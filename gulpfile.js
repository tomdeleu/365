var postcss = require('gulp-postcss');
var gulp = require('gulp');
var sourcemaps = require('gulp-sourcemaps');
var sass = require('gulp-sass');
var cssnano = require('gulp-cssnano');

gulp.task('sass', function () {
    return gulp.src('./sass/[^_]*.scss')
        .pipe(sourcemaps.init())
        .pipe(sass({
            includePaths: require("bourbon-neat").includePaths
        }).on('error', sass.logError))
        .pipe(cssnano())
        .pipe(sourcemaps.write('.'))
        .pipe(gulp.dest('./dist'));
});

gulp.task('watch', function() {
    gulp.watch('sass/**/*.scss', ['sass']);
});
