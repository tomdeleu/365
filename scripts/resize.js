const sharp = require("sharp")
const fs = require('fs');
const path = require("path")

var months = ["01"]
var files = [];

months.forEach((month) => {
    var folder = path.join(__dirname, "../img", month)
    fs.readdir(folder, (err, files) => {
        files.forEach(file => {
            if (file.indexOf("_t") == -1) {

                var fileName = path.join(__dirname, "../img", month, file)
                files.push(new Promise((resolve, reject) => {
                    var n = file.slice(0, -4)
                    var outName = path.join(__dirname, "../img", month, (n + "_t.jpg"))
                    sharp(fileName)
                        .resize(200, 200)
                        .toFile(outName, function(err) {
                            // output.jpg is a 300 pixels wide and 200 pixels high image
                            // containing a scaled and cropped version of input.jpg
                            console.info("done file", outName)
                            resolve();
                        });
                }))
            }
        });
    })
})

Promise.all(files).then(() => {
    console.info("DONE")
})